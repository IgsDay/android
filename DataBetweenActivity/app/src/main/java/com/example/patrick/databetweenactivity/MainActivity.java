package com.example.patrick.databetweenactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

    private final int REQUEST_CODE = 443;

    EditText textMain;
    Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Definition du contexte
        this.context = this;

        //Recuperation des elements de la vue
        this.textMain = (EditText) findViewById(R.id.editTxtMain);

        final Button btnMain = (Button) findViewById(R.id.btnMain);

        btnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SecondActivity.class);

                //Tableau cle/valeur contenant les proprites a envoyer aux activités suivantes
                Bundle bundleData = new Bundle();

                //Préparation des valeurs du Bundle
                bundleData.putString("info", textMain.getText().toString());

                //Lancement de l'activité

                //Creation et ajout de mon Dylan
                Personne dylan = new Personne("Dylan","Ligneul",21);
                bundleData.putParcelable("dylan", dylan);

                //Passage du Bundle dans l'intent
                intent.putExtras(bundleData);

                context.startActivityForResult(intent,REQUEST_CODE);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Si le résultat provient d’une demande de la fenêtre1
        if (requestCode == 443) {
            // le code retour est bon
           if (resultCode == 200) { //récupérer les informations
               Bundle dataReceive = data.getExtras();
                Log.e("message " ,dataReceive.getString("returnedInfo"));
               //et les afficher dans TextView
               this.textMain.setText(dataReceive.getString("returnedInfo"));
            }
          }
    }


}
