package com.example.patrick.databetweenactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


class SecondActivity extends Activity {
    private final int REQUEST_CODE = 80;
    private final int RESULT_CODE = 200;

    Activity context;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE){
            finish();
        }
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.second_activity);

        this.context = this;
        context.setTitle("Votre texte");

        final EditText text = (EditText) findViewById(R.id.editSecond);
        final EditText nom = (EditText) findViewById(R.id.nom);
        final EditText prenom = (EditText) findViewById(R.id.prenom);
        final EditText age = (EditText) findViewById(R.id.age);

        final Button btn = (Button)  findViewById(R.id.btnSecond);
        final Button nextActivity = (Button)  findViewById(R.id.button);

        Bundle bundleReceive = this.getIntent().getExtras();

        String textGiven = bundleReceive.getString("info");

        text.setText(textGiven);

        Personne person = bundleReceive.getParcelable("dylan");

        nom.setText(person.getNom());
        prenom.setText(person.getPrenom());
        age.setText(String.valueOf(person.getAge()));

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle returnedData = new Bundle();

                returnedData.putString("returnedInfo", text.getText().toString());
                
                context.getIntent().putExtras(returnedData);

                setResult(RESULT_CODE, context.getIntent());

                //Destruction de la fenetre actuelle
                finish();
            }
        });

        nextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ThirdActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
            }
        });


    }

}
