package com.example.patrick.multipleactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {


    private Button mButton;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton = (Button)findViewById(R.id.button);
        mButton.setOnClickListener(this);

    }


    public void onClick(View v) {
        if (v == mButton) {
            /*
             * Nous sommes maintenant sûr que la vue ayant été cliquée est le bouton
             * de notre interface. Il suffit donc de créer un nouvel Intent pour démarrer
             * la seconde activité.
             */

            //Definition de notre intention envers notre activité suivante
            Intent intent = new Intent(this, ActiviteSuivante.class);

            //Lancement de l'activité
            startActivity(intent);
        }
    }

}
