package com.example.patrick.multipleactivity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class ActiviteSuivante extends Activity {

    private TextView text;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activite_suivante);
        this.text = findViewById(R.id.title);
        this.text.setText("Bienvenu dans votre nouvelle activité");
        this.text.setTextColor(Color.GREEN);
    }
}
