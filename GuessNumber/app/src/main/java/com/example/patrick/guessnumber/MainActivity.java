package com.example.patrick.guessnumber;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.HashMap;

public class MainActivity extends Activity implements View.OnClickListener {

    private HashMap<Integer,String> buttonsId = new HashMap<>();

    private int randNumber;
    private String numberTyped = "";

    private TextView screen,result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.fillButtonList();

        double randomDouble = Math.random();
        randomDouble *= 100;
        this.randNumber = (int)randomDouble;


        this.screen = findViewById(R.id.textView);
        this.result = findViewById(R.id.buttonResult);

        this.result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setText("");
                screen.setText("");
                numberTyped = "";
            }
        });

        for (int id : this.buttonsId.keySet()) {
            findViewById(id).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        int parsedNumber;
        String valueTouched =  this.buttonsId.get(v.getId());
        this.numberTyped = this.numberTyped.concat(valueTouched);
        parsedNumber = Integer.parseInt(this.numberTyped);
        this.guessAttempt(parsedNumber);
        this.showScreen(this.numberTyped);
    }

    private void showScreen(String value) {
        this.screen.setText(value);
    }

    private void guessAttempt(int value) {
        if (value > this.randNumber){
            this.showResult(value, '-');
        }else {
            this.showResult(value, 'V');
        }
    }

    private void showResult(int value, char c) {

        switch (c){
            case '+':
                this.result.setTextColor(Color.RED);
                break;
            case '-':
                this.result.setTextColor(Color.BLUE);
                break;
            case 'v':
                this.result.setTextColor(Color.GREEN);
                c = ' ';
                break;
        }

        this.result.setTextSize(50);
        this.result.setText(c+" "+value);
    }

    private void fillButtonList() {
        this.buttonsId.put(R.id.imageButton0,"0");
        this.buttonsId.put(R.id.imageButton1,"1");
        this.buttonsId.put(R.id.imageButton2,"2");
        this.buttonsId.put(R.id.imageButton3,"3");
        this.buttonsId.put(R.id.imageButton4,"4");
        this.buttonsId.put(R.id.imageButton5,"5");
        this.buttonsId.put(R.id.imageButton6,"6");
        this.buttonsId.put(R.id.imageButton7,"7");
        this.buttonsId.put(R.id.imageButton8,"8");
        this.buttonsId.put(R.id.imageButton9,"9");
    }


}
